import {Component, Input} from '@angular/core';
import {ElectionService} from './election.service';

@Component({
  selector: 'election',
  template: require('./election.html'),
  providers: [ElectionService]
})
export class ElectionComponent {
  input = {
    title: 'Testing',
    candidates: 'fred'
  };
  output = 'this is the output';

  log(message: string) {
  this.output += `\n=+ ${message}`;
  }

  constructor(private election: ElectionService) { }
  go() {
    var message = this.election.sayHello();
    this.log(message);
  }
}

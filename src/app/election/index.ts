import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import {ElectionComponent} from './election';

// ngModule is a decorator function that takes a single metadata object whose properties describe the module. The most important properties are:
// declarations - view classes of module: components, directives, and pipes.
// exports - subset of declarations usable in templates of other modules.
// imports - modules with exported classes needed by this modules templates.
// providers - creators of services that this module contributes to the global collection of services; accessible throughout app.
// bootstrap - in root module the root component that hosts all other views.

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule
  ],
  declarations: [
    ElectionComponent
  ],
  exports: [
    ElectionComponent
  ],
})
export class ElectionModule {}

export const TestData {
  parties: [
    'Red', 'Blue', 'Yellow', 'Green',
    'U-1', 'U-2'],
  candidates: ['RUBY,R', 'CRIMSON,C', 'BURGANDY,B', 'CYAN,C', 'NAVY,N', 'BLONDE,B', 'MUSTARD,M', 'CANARY,C', 'HONEY,H', 'EMERALD,E', 'PINE,P', 'MINT,M', 'SILVER,S', 'GREY,G'],
  ticket: {
    'Red': ['RUBY,R', 'CRIMSON,C', 'BURGANDY,B'],
    'Blue': ['CYAN,C', 'NAVY,N'],
    'Yellow': ['BLONDE,B', 'MUSTARD,M', 'CANARY,C', 'HONEY,H'],
    'Green': ['EMERALD,E', 'PINE,P', 'MINT,M'],
    'U-1': ['SILVER,S'],
    'U-2': ['GREY,G']
  }

}
